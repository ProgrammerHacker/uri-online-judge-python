n = int(input())

while n > 0:
    n1, n2, n3 = map(float, input().split())
    ponderada = ((n1 * 2) + (n2 * 3) + (n3 * 5)) / 10
    print("%.1f" % ponderada)
    n -= 1
