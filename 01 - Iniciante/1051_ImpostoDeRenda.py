salario = float(input())

if salario <= 2000:
    print("Isento")
elif salario < 3000:
    salario = (salario - 2000) * (8 / 100)
    print("R$ %.2f" % salario)
elif salario < 4500:
    salario = (1000 * (8 / 100)) + (salario - 3000) * (18 / 100)
    print("R$ %.2f" % salario)
else:
    salario = (1000 * (8 / 100)) + (1500 * (18 / 100)) + (salario - 4500) * (28 / 100)
    print("R$ %.2f" % salario)