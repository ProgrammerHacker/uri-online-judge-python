valor1, valor2, valor3 = map(float, input().split())

if valor1 + valor2 > valor3 and valor2 + valor3 > valor1 and valor1 + valor3 > valor2:
    perimetro = valor1 + valor2 + valor3
    print("Perimetro = %.1f" % perimetro)
else:
    area = (valor3 * (valor1 + valor2)) / 2
    print("Area = %.1f" % area)