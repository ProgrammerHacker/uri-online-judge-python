valor1, valor2, valor3, valor4 = map(int, input().split())

hora = (valor3 - valor1);
if (valor3 - valor1) < 0:
    hora = 24 + (valor3 - valor1)

minuto = (valor4 - valor2)
if (valor4 - valor2) < 0:
    minuto = 60 + (valor4 - valor2)
    hora = hora - 1

if valor3 == valor1 and valor4 == valor2:
    print("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)")
else:
    print("O JOGO DUROU %d" % hora + " HORA(S) E %d MINUTO(S)" % minuto)
