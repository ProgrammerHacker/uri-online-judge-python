import math

valores = input().split()
valor1, valor2, valor3 = valores

a = float(valor1)
b = float(valor2)
c = float(valor3)

delta = (b**2) - (4*a*c)
if delta <= 0 or a == 0:
    print("Impossivel calcular")
else:
    r1 = ((-1*b) + math.sqrt(delta))/(2*a)
    r2 = ((-1*b) - math.sqrt(delta))/(2*a)
    print("R1 = %0.5f" % r1)
    print("R2 = %0.5f" % r2)