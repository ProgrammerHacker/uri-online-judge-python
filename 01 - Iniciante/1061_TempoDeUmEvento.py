d1 = input().split()
h1 = input().split(" : ")
d2 = input().split()
h2 = input().split(" : ")

dia = int(d2[1]) - int(d1[1])
hora = int(h2[0]) - int(h1[0])
minuto = int(h2[1]) - int(h1[1])
segundo = int(h2[2]) - int(h1[2])

if segundo < 0:
    segundo += 60
    minuto = minuto - 1

if minuto < 0:
    minuto += 60
    hora = hora - 1

if hora < 0:
    hora += 24
    dia = dia - 1

print("%i dia(s)" % dia)
print("%i hora(s)" % hora)
print("%i minuto(s)" % minuto)
print("%i segundo(s)" % segundo)
