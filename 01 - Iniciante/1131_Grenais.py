continuar = 1
grenais = 0
empate = 0
inter = 0
gremio = 0

while (continuar == 1):
    n1, n2 = map(int, input().split())
    grenais +=1

    if (n1 == n2): empate +=1
    if (n1 > n2): inter +=1
    else: gremio +=1

    continuar = int(input("Novo grenal (1-sim 2-nao)\n"))

print("%i grenais" %grenais)
print("Inter:%i" %inter)
print("Gremio:%i" %gremio)
print("Empates:%i" %empate)

if (inter > gremio):
    print("Inter venceu mais")
elif (gremio > inter):
    print("Gremio venceu mais")
else:
    print("Nao houve vencedor")