salario = float(input())

if salario >= 0 and salario <= 400:
    salarioFinal = salario + (salario * (15 / 100))
    print("Novo salario: %.2f" % salarioFinal)
    print("Reajuste ganho: %.2f" % (salario * (15 / 100)))
    print("Em percentual: 15 %")
elif salario > 400 and salario <= 800:
    salarioFinal = salario + (salario * (12 / 100))
    print("Novo salario: %.2f" % salarioFinal)
    print("Reajuste ganho: %.2f" % (salario * (12 / 100)))
    print("Em percentual: 12 %")
elif salario > 800 and salario <= 1200:
    salarioFinal = salario + (salario * (10 / 100))
    print("Novo salario: %.2f" % salarioFinal)
    print("Reajuste ganho: %.2f" % (salario * (10 / 100)))
    print("Em percentual: 10 %")
elif salario > 1200 and salario <= 2000:
    salarioFinal = salario + (salario * (7 / 100))
    print("Novo salario: %.2f" % salarioFinal)
    print("Reajuste ganho: %.2f" % (salario * (7 / 100)))
    print("Em percentual: 7 %")
elif salario > 2000:
    salarioFinal = salario + (salario * (4 / 100))
    print("Novo salario: %.2f" % salarioFinal)
    print("Reajuste ganho: %.2f" % (salario * (4 / 100)))
    print("Em percentual: 4 %")