n = 0
m = 1
print("I=%i" % n, "J=%i" % m)
print("I=%i" % n, "J=%i" % (m + 1))
print("I=%i" % n, "J=%i" % (m + 2))

while n < 0.8:
    n += 0.2
    m += 0.2
    print("I=%.1f" % n, "J=%.1f" %m)
    print("I=%.1f" % n, "J=%.1f" %(m+1))
    print("I=%.1f" % n, "J=%.1f" %(m + 2))

n = 1
m = 2
print("I=%i" % n, "J=%i" % m)
print("I=%i" % n, "J=%i" % (m + 1))
print("I=%i" % n, "J=%i" % (m + 2))

while n < 1.6:
    n += 0.2
    m += 0.2
    print("I=%.1f" % n, "J=%.1f" %m)
    print("I=%.1f" % n, "J=%.1f" %(m+1))
    print("I=%.1f" % n, "J=%.1f" %(m + 2))

n = 2
m = 3
print("I=%i" % n, "J=%i" % m)
print("I=%i" % n, "J=%i" % (m + 1))
print("I=%i" % n, "J=%i" % (m + 2))