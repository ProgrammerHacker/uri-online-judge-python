valor1, valor2 = map(int, input().split())

if valor2 % valor1 == 0 or valor1 % valor2 == 0:
    print("Sao Multiplos")
else:
    print("Nao sao Multiplos")