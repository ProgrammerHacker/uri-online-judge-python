valores = input().split()
valor1, valor2, valor3 = valores

triangulo = (float(valor1) * float(valor3))/2
circulo = 3.14159 * (float(valor3) * float(valor3))
trapezio = float(valor3) * (float(valor1) + float(valor2))/2
quadrado = float(valor2) * float(valor2)
retangulo = float(valor1) * float(valor2)

print("TRIANGULO: %0.3f" % triangulo)
print("CIRCULO: %0.3f" % circulo)
print("TRAPEZIO: %0.3f" % trapezio)
print("QUADRADO: %0.3f" % quadrado)
print("RETANGULO: %0.3f" % retangulo)