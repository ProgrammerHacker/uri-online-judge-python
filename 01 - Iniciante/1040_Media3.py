valores = input().split()
nota1, nota2, nota3, nota4 = valores

media = ((float(nota1) * 2) + (float(nota2) * 3) + (float(nota3) * 4) + (float(nota4) * 1)) / 10
print("Media: %.1f"%media)

if media >= 7:
    print("Aluno aprovado.")
elif media < 5:
    print("Aluno reprovado.")
elif 5 <= media < 7.0:
    print("Aluno em exame.")

    final = float(input())
    print("Nota do exame: %0.1f" % final)
    mediaFinal = (media + final) / 2

    if mediaFinal >= 5:
        print("Aluno aprovado.")
    else:
        print("Aluno reprovado")

    print("Media final: %0.1f" % mediaFinal)
