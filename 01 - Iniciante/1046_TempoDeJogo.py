valor1, valor2 = map(int, input().split())

hora = valor2 - valor1

if (valor2 - valor1) < 0:
    hora = 24 + (valor2 - valor1)
    print("O JOGO DUROU %d HORA(S)" % hora)
elif valor2 == valor1:
    print("O JOGO DUROU 24 HORA(S)")
else:
    print("O JOGO DUROU %d HORA(S)" % hora)