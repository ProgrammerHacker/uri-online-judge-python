n = int(input())

while n > 0:
    n1, n2 = map(int, input().split())

    if n1 > n2:
        n1, n2 = n2, n1

    soma = 0

    while n1 < n2:
        n1 +=1
        if n1 == n2:
            break
        elif n1 % 2 == 1:
            soma = soma + n1

    print(soma)
    n -= 1
