n1, n2, n3 = map(int, input().split())

valor1 = n1
valor2 = n2
valor3 = n3

if valor1 < valor2:
    aux = valor1
    valor1 = valor2
    valor2 = aux
if valor2 < valor3:
    aux = valor2
    valor2 = valor3
    valor3 = aux
if valor1 < valor2:
    aux = valor1
    valor1 = valor2
    valor2 = aux

print(valor3)
print(valor2)
print(valor1)
print()
print(n1)
print(n2)
print(n3)