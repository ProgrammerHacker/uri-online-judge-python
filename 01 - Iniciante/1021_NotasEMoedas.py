valor = float(input())

cem = valor/100
cinquenta = (valor%100)/50
vinte = ((valor%100)%50)/20
dez = (((valor%100)%50)%20)/10
cinco = ((((valor%100)%50)%20)%10)/5
dois = (((((valor%100)%50)%20)%10)%5)/2
um = ((((((valor%100)%50)%20)%10)%5)%2)/1

valor = (valor*100)%100
cinquentaCentavos = valor/50
valor = valor%50
vinteCincoCentavos = valor/25
valor = valor%25
dezCentavos = valor/10
valor = valor%10
cincoCentavos = valor/5
valor = valor%5
umCentavo = valor/1

print("NOTAS:")
print("%d nota(s) de R$ 100.00" %cem)
print("%d nota(s) de R$ 50.00" %cinquenta)
print("%d nota(s) de R$ 20.00" %vinte)
print("%d nota(s) de R$ 10.00" %dez)
print("%d nota(s) de R$ 5.00" %cinco)
print("%d nota(s) de R$ 2.00" %dois)
print("MOEDAS:")
print("%d moeda(s) de R$ 1.00" %um)
print("%d moeda(s) de R$ 0.50" %cinquentaCentavos)
print("%d moeda(s) de R$ 0.25" %vinteCincoCentavos)
print("%d moeda(s) de R$ 0.10" %dezCentavos)
print("%d moeda(s) de R$ 0.05" %cincoCentavos)
print("%d moeda(s) de R$ 0.01" %umCentavo)