positivo = 0
negativo = 0
par = 0
impar = 0
for i in range(5):
    n = float(input())
    if n > 0:
        positivo = positivo + 1
    elif n < 0:
        negativo = negativo + 1
    if n % 2 == 0:
        par = par + 1
    else:
        impar = impar + 1

print("%i valor(es) par(es)" % par)
print("%i valor(es) impar(es)" % impar)
print("%i valor(es) positivo(s)" % positivo)
print("%i valor(es) negativo(s)" % negativo)