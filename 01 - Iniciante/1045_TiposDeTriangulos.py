valor1, valor2, valor3 = map(float, input().split())

if valor1 < valor2:
    aux = valor1
    valor1 = valor2
    valor2 = aux
if valor2 < valor3:
    aux = valor2
    valor2 = valor3
    valor3 = aux
if valor1 < valor2:
    aux = valor1
    valor1 = valor2
    valor2 = aux

if valor1 >= (valor2 + valor3):
    print("NAO FORMA TRIANGULO")
else:
    if (valor1 ** 2) == (valor2 ** 2) + (valor3 ** 2):
        print("TRIANGULO RETANGULO")
    if (valor1 ** 2) > (valor2 ** 2) + (valor3 ** 2):
        print("TRIANGULO OBTUSANGULO")
    if (valor1 ** 2) < (valor2 ** 2) + (valor3 ** 2):
        print("TRIANGULO ACUTANGULO")
    if valor1 == valor2 == valor3:
        print("TRIANGULO EQUILATERO")
    if valor1 == valor2 and valor1 != valor3 or valor2 == valor3 and valor2 != valor1 or valor3 == valor1 and valor3 != valor2:
        print("TRIANGULO ISOSCELES")