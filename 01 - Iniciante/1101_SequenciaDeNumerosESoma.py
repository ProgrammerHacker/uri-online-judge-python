n1, n2 = map(int, input().split())

while (n1>0 and n2>0):
    sequencia = ""
    soma = 0
    if(n1>n2): n1, n2 = n2, n1

    for i in range(n1,(n2+1)):
        sequencia += str(i)+" "
        soma = soma+i
    print(sequencia+"Sum=%i"%soma)
    n1, n2 = map(int, input().split())