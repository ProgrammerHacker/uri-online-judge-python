nome1 = input()
nome2 = input()
nome3 = input()

if str(nome1) == "vertebrado":
    if str(nome2).lower() == "ave":
        if str(nome3).lower() == "carnivoro":
            print("aguia")
        elif str(nome3).lower() == "onivoro":
            print("pomba")
    elif str(nome2).lower() == "mamifero":
        if str(nome3).lower() == "onivoro":
            print("homem")
        elif str(nome3).lower() == "herbivoro":
            print("vaca")
elif str(nome1).lower() == "invertebrado":
    if str(nome2).lower() == "inseto":
        if str(nome3).lower() == "hematofago":
            print("pulga")
        elif str(nome3).lower() == "herbivoro":
            print("lagarta")
    elif str(nome2).lower() == "anelideo":
        if str(nome3).lower() == "hematofago":
            print("sanguessuga")
        elif str(nome3).lower() == "onivoro":
            print("minhoca")