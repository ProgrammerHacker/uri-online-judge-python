positivo = 0
soma = 0.0
for i in range(6):
    n = float(input())
    if n > 0:
        positivo = positivo + 1
        soma = soma + n

media = soma / float("%.1f" % positivo)

print("%i valores positivos" % positivo)
print("%.1f" % media)
