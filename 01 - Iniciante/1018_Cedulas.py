valor = int(input())

cem = valor/100
cinquenta = (valor%100)/50
vinte = ((valor%100)%50)/20
dez = (((valor%100)%50)%20)/10
cinco = ((((valor%100)%50)%20)%10)/5
dois = (((((valor%100)%50)%20)%10)%5)/2
um = ((((((valor%100)%50)%20)%10)%5)%2)/1

print(valor)
print("%d nota(s) de R$ 100,00" %cem)
print("%d nota(s) de R$ 50,00" %cinquenta)
print("%d nota(s) de R$ 20,00" %vinte)
print("%d nota(s) de R$ 10,00" %dez)
print("%d nota(s) de R$ 5,00" %cinco)
print("%d nota(s) de R$ 2,00" %dois)
print("%d nota(s) de R$ 1,00" %um)