n = int(input())

quant = 0
total = 0
coelho = 0
sapo = 0
rato = 0

while n > 0:
    quant, tipo = input().split()

    quant = int(quant)

    if str(tipo) == "C":
        coelho = coelho + quant
    if str(tipo) == "S":
        sapo = sapo + quant
    if str(tipo) == "R":
        rato = rato + quant

    total = total + quant
    n -= 1

print("Total: %i" % total + " cobaias")
print("Total de coelhos: %i" % coelho)
print("Total de ratos: %i" % rato)
print("Total de sapos: %i" % sapo)
print("Percentual de coelhos: %.2f" % ((coelho / total) * 100)+" %")
print("Percentual de ratos: %.2f" % ((rato / total) * 100)+" %")
print("Percentual de sapos: %.2f" % ((sapo / total) * 100)+" %")
